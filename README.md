# Locutus.of.Borg: a GraalVM FaaS runtime for Knative[Eventing]

🚧

## References

- [Compile and Run a Polyglot Application](https://www.graalvm.org/docs/reference-manual/embed/)
- [Initial work (POC)](https://github.com/bots-garden/unimatrix-zero)
- [Unimatrix 0](https://memory-alpha.fandom.com/wiki/Unimatrix_Zero)
- [7of9](https://memory-alpha.fandom.com/wiki/Seven_of_Nine)
