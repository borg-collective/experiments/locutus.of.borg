package garden.bots.unimatrix

import arrow.core.Either
import io.cloudevents.core.format.EventFormat
import io.cloudevents.core.message.Message
import io.cloudevents.core.message.StructuredMessage
import io.cloudevents.http.vertx.VertxHttpServerResponseMessageVisitor
import io.cloudevents.http.vertx.VertxMessageFactory
import io.cloudevents.jackson.JsonFormat
import io.vertx.core.AbstractVerticle
import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.http.HttpServer
import io.vertx.core.http.HttpServerRequest
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj


class MainVerticle : AbstractVerticle() {

  override fun stop(stopFuture: Future<Void>) {
    super.stop()
  }

  //Receiving CloudEvents // startPromise: Promise<Void>
  override fun start() {
    val kompilo = Kompilo()
    val language = System.getenv("LANG") ?: "js"
    val httpPort = System.getenv("PORT")?.toInt() ?: 8080
    val welcome = System.getenv("WELCOME") ?: "👋 Hello World 🌍"
    val contentType = System.getenv("CONTENT_TYPE") ?: "application/json;charset=UTF-8"

    val functionName = System.getenv("FUNCTION_NAME") ?: "hello"
    val functionCode = System.getenv("FUNCTION_CODE") ?: """
      function ${functionName}(params) {
        return {
          message: "👋 Hello World 🌍",
        }
      }
    """.trimIndent()

    val compiledFunction = kompilo.compileFunction(functionCode, language)

    compiledFunction.let {
      when(it) {
        is Either.Left -> { // compilation error
          println("😡 ${it.a.message}")
        }
        is Either.Right -> { // compilation is OK, and name of the function to invoke is "handle"
          println("😀")
        }
      }
    }
    /*
            // call the function
            kompilo.invokeFunction(functionName, params).let {
              when(it) {
                is Either.Left -> { // execution error
                  context.response().putHeader("content-type", "application/json;charset=UTF-8")
                    .end(
                      json {
                        obj("error" to it.a.message)
                      }.encodePrettily()
                    )
                }
                is Either.Right -> { // execution is OK
                  val result = it.b
                  context.response().putHeader("content-type", contentType)
                    .end(result.toString())
                }
              }
            }
     */

    vertx.createHttpServer()
      .requestHandler { req ->
        VertxMessageFactory.fromHttpServerRequest(req)
          .onComplete { asyncResult ->
            when {
              // If decoding succeeded, we should write the event back
              asyncResult.failed() -> {
                val event = asyncResult.result().toEvent()

                println("🎃 ${event.data.toString()}")

                // Echo the message, as structured mode
                StructuredMessage.fromEvent(JsonFormat(), event)
                  .visit(VertxHttpServerResponseMessageVisitor.create(req.response()))
              }
              asyncResult.succeeded() -> {
                req.response().setStatusCode(500).end()
              }
            }
          }
      }
      .listen(httpPort) { http ->
        when {
          http.failed() -> {
            //startPromise.fail(http.cause())
          }
          http.succeeded() -> {
            println("🤖 Locutus of Borg, GraalVM runtime for $functionName function started on port $httpPort")
            println(welcome)
            //startPromise.complete()
          }
        }
      }
  }
}
